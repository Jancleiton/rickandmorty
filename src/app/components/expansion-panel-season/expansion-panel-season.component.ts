import { Component, Input, OnInit } from '@angular/core';
import { Episode } from 'src/app/interfaces/Episode';
import { ListData } from 'src/app/interfaces/ListData';
import { EpisodeService } from 'src/app/services/episode.service';

@Component({
  selector: 'expansion-panel-season',
  templateUrl: './expansion-panel-season.component.html',
  styleUrls: ['./expansion-panel-season.component.sass']
})
export class ExpansionPanelSeasonComponent implements OnInit {

  @Input() title: string = ''
  episodes: Episode[] = []
  displayedColumns: string[] = ['air_date', 'title', 'characters'];

  constructor(
    private episodeService: EpisodeService
  ) { }

  ngOnInit(): void {
    this.getEpisodes()
  }

  getEpisodes(){
    let season = this.title.split(' ')[1]    
    this.episodeService.getEpisodesBySeason(season)
      .subscribe((lista: ListData<Episode>) => this.episodes = lista.results)
  }

}
