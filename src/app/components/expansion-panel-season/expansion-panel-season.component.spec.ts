import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ExpansionPanelSeasonComponent } from './expansion-panel-season.component';

describe('ExpansionPanelSeasonComponent', () => {
  let component: ExpansionPanelSeasonComponent;
  let fixture: ComponentFixture<ExpansionPanelSeasonComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ExpansionPanelSeasonComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ExpansionPanelSeasonComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
