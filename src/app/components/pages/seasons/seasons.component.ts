import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-seasons',
  templateUrl: './seasons.component.html',
  styleUrls: ['./seasons.component.sass']
})
export class SeasonsComponent implements OnInit {

  seasons = [
    'Season 01',
    'Season 02',
    'Season 03',
    'Season 04',
    'Season 05',    
  ]

  constructor() { }

  ngOnInit(): void {    
  }  

}
