export interface InfoListData {
    count: number,
    pages: number,
    next: string
    prev: string
}