import { InfoListData } from "./InfoListData";

export interface ListData<T> {
    info: InfoListData
    results: T[]
}