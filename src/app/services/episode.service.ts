import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Episode } from '../interfaces/Episode';
import { ListData } from '../interfaces/ListData';

@Injectable({
  providedIn: 'root'
})
export class EpisodeService {

  constructor(
    private http: HttpClient
  ) { }

  getEpisodesBySeason(season: string): Observable<ListData<Episode>>{
    return this.http.get<ListData<Episode>>(`https://rickandmortyapi.com/api/episode/?episode=s${season}`)
  }
}
